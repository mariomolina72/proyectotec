var form = document.querySelector('#comment-form');
var textarea = document.querySelector('#comment-textarea');
var commentContainer = document.querySelector('.comment-container');

function addComment(comentario) {
  var newComment = `
    <div class="comment">
      <div class="user">
        <i class="far fa-user"></i>
        <p class="author">Ricardo Velasco</p>
      </div>
      <p class="description">${comentario}</p>
    </div>`

  var comment = document.createElement('div');
  comment.classList.add('comment');
  var user = document.createElement('div');
  user.classList.add('user');
  comment.append(user);

  commentContainer.innerHTML = newComment + commentContainer.innerHTML;
}

form.addEventListener('submit', function(evento) {
  evento.preventDefault();
  var comentario = textarea.value;
  console.log(comentario);
  textarea.value = null;
  addComment(comentario);
});

//ir a traer las estrellas
var stars = document.querySelectorAll('.fa-star');

//agregar un evento de click a cada estrella
stars.forEach(function(elemento) {

  //cuando el usuario haga click cambiar la calse de la estrela
  elemento.addEventListener('click', function(){
    this.classList.replace('far', 'fas');
  })
});

